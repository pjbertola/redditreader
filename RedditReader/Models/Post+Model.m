//
//  Post+Model.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "Post+Model.h"
#import "CoreDataManager.h"

@implementation Post (Model)

+(instancetype)factory {
    NSManagedObjectContext *context = [CoreDataManager sharedManager].persistentContainer.viewContext;
    Post *post = [NSEntityDescription insertNewObjectForEntityForName:@"Post" inManagedObjectContext:context];
    return post;
}
@end
