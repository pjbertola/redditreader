//
//  Post+Model.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "Post+CoreDataClass.h"

@interface Post (Model)
+(instancetype)factory;
@end
