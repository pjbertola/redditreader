//
//  PostTableViewCell.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/15/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//
@class PostModelView;
#import <UIKit/UIKit.h>


/**
 El protocolo es solo de muestra. En el caso de que la celda tuviera botones se resolvería con esto. Por eso solo muestra cuando se selecciona la celda. En otros lugares podría haber usado protocolos, pero considere que se resolvía mejor con bloques.
 */
@protocol PostTableViewCellDelegate <NSObject>

-(void)tapButton:(PostModelView *)postMV;

@end

@interface PostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *authorLb;
@property (weak, nonatomic) IBOutlet UILabel *commentsLb;
@property (weak, nonatomic) IBOutlet UILabel *subredditLb;
@property (weak, nonatomic) IBOutlet UILabel *dateLb;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;

@property (weak, nonatomic) id<PostTableViewCellDelegate> delegate;

-(void)setModel:(PostModelView *)postMV;

@end
