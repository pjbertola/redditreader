//
//  PostTableViewCell.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/15/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "PostTableViewCell.h"
#import "PostModelView.h"
#import "UIImageView+AFNetworking.h"
#import "Utils.h"
#import "Defines.h"

@interface PostTableViewCell(){
    PostModelView *_postMV;
}
@end
@implementation PostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initializer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    [self.delegate tapButton:_postMV];
}
-(void)prepareForReuse {
    [self initializer];
}
-(void)initializer {
    self.imageView.image = [UIImage imageNamed:IMG_PLACEHOLDER];
    self.authorLb.text = @"";
    self.commentsLb.text = @"";
    self.subredditLb.text = @"";
    self.dateLb.text = @"";
    self.titleLb.text = @"";
    self.imageView.layer.cornerRadius = 5.0f;
    self.imageView.clipsToBounds = YES;
}

-(void)setModel:(PostModelView *)postMV {
    _postMV = postMV;
    
    if ([Utils hasConnection]) {
        [self.imageView setImageWithURL:[NSURL URLWithString:_postMV.thumbnail] placeholderImage:[UIImage imageNamed:IMG_PLACEHOLDER]];
        
    } else {
        NSURL *urlPath = [NSURL fileURLWithPath:[Utils getFilePathName:postMV.thumbnail]];
        [self.imageView setImageWithURL:urlPath placeholderImage:[UIImage imageNamed:IMG_PLACEHOLDER]];
    }
    self.authorLb.text = _postMV.author;
    self.commentsLb.text = [NSString stringWithFormat:@"%@ %hd", NSLocalizedString(@"Comments", nil), _postMV.num_comments];
    self.subredditLb.text = _postMV.subreddit;
    self.titleLb.text = _postMV.title;
    self.dateLb.text = [Utils formatDateForLabel:_postMV.created_utc];
    
}

@end
