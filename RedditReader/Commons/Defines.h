//
//  Defines.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/18/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

#define BASE_URL @"https://www.reddit.com"
#define IMG_PLACEHOLDER @"reddit_placeholder"

#endif /* Defines_h */
