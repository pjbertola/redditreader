//
//  Utils.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/17/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

//Connectivity
+ (BOOL) hasConnection;

//Date
+(NSString *)formatDateForLabel:(NSDate *)date;

//File System
+ (NSString *)getFilePathName:(NSString *)name;
+ (void)writeToFileSystem:(NSData*)file withName:(NSString*)name;
@end
