//
//  Utils.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/17/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "Utils.h"
#import <AFNetworking/AFNetworking.h>

@implementation Utils

#pragma mark - Connection Methods

/**
 Check if the app has connection to internet.

 @return a bool than indicates if the app has internet connection.
 */
+ (BOOL) hasConnection{
    return !([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable);
}

#pragma mark - Date Methods

/**
 Trnasform a NSDdate to string. If the date is from the day the app is running, shows hours and minutes; if not, shows a full date

 @param date NSdate object
 @return returns a formatted string.
 */
+(NSString *)formatDateForLabel:(NSDate *)date{
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:date];
    NSString *dateStr;
    NSDateFormatter *dateF = [[NSDateFormatter alloc]init];
    if (!today) {

        [dateF setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"dd MMM yyyy" options:0 locale:[NSLocale currentLocale]]];
        dateStr = [dateF stringFromDate:date];
        
    } else {
        [dateF setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"hh:mm a" options:0 locale:[NSLocale currentLocale]]];
        dateStr = [dateF stringFromDate:date];
    }
    return dateStr;
}

#pragma mark - FileSystem Methods

/**
 Converts a url to a temporary file system url.

 @param name the url string of the image.
 @return return a string of a temporary file system url.
 */
+ (NSString *)getFilePathName:(NSString *)name{
    NSString *newName = [name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    newName = [newName stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *tempPath = NSTemporaryDirectory();
    NSString *tempFile = [tempPath stringByAppendingPathComponent:newName];
    
    return tempFile;
}


/**
 Write a file in the given location.

 @param file NSData to write.
 @param name of the file in the system.
 */
+ (void)writeToFileSystem:(NSData*)file withName:(NSString*)name{
    NSString *tempFile = [Utils getFilePathName:name];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:tempFile];
    if(!fileExists) {
        [file writeToFile:tempFile atomically:YES];
    }
}
@end
