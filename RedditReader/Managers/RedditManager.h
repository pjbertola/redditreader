//
//  RedditManager.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionManager.h"

@interface RedditManager : NSObject
+ (instancetype _Nonnull )sharedManager;
-(void)getPostAfter:(NSString *_Nullable)after withBlock:(nullable void (^)(NSMutableArray*  _Nullable list, NSString* _Nullable after, NSError* _Nullable error))finishFunction;
@end
