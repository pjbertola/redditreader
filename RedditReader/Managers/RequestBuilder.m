//
//  RequestBuilder.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/15/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "RequestBuilder.h"
#import <AFNetworking/AFNetworking.h>


@implementation RequestBuilder


/**
 Returns a request to perform a get posts.

 @param after Is the last post id. Its use to get the next page.
 @return the NSMutableURLRequest to get the posts.
 */
+(NSMutableURLRequest*_Nullable) createGetPostsAfter:(NSString *_Nullable)after
{
    NSString *url = @"https://www.reddit.com/top/.json";
    
    if (after) {
        url = [url stringByAppendingString:[NSString stringWithFormat:@"?after=%@",after]];
    }
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:nil error:nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    return request;
}


@end
