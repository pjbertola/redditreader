//
//  CoreDataManager.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "CoreDataManager.h"

@implementation CoreDataManager

+ (instancetype)sharedManager {
    static CoreDataManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CoreDataManager alloc] init];
    });
    
    return sharedInstance;
}

-(instancetype)init {
    self = [super init];
    if (self)
    {
        [self persistentContainer];
    }
    return self;
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"RedditReader"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - User Methods

/**
 Deletes all post objects.
 */
-(void)deleteAllPost {
    [self deleteAllRecordsForEntity:@"Post"];
}


/**
 Deletes all records of an entity

 @param entityName Name of the entity.
 */
-(void)deleteAllRecordsForEntity:(NSString*)entityName {
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    NSError* error = nil;
    [self.persistentContainer.persistentStoreCoordinator executeRequest:delete withContext:self.persistentContainer.viewContext error:&error];
    
}

/**
 Deletes one record.

 @param object record to delete.
 */
- (void)deleteObject:(NSManagedObject*)object{
    [self.persistentContainer.viewContext deleteObject:object];
}

/**
 Retrieves all post objects from core data.

 @return post objects list.
 */
- (NSArray *)getAllPosts{
    return [self getAllRecordsForEntity:@"Post"];
}

/**
 Retrieves all entity objects from core data.

 @param entityName Name of the entity.
 @return list of entity objetcs
 */
- (NSArray *)getAllRecordsForEntity:(NSString *)entityName {
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    NSError* error = nil;
    NSArray* results = [self.persistentContainer.viewContext executeFetchRequest:request error:&error];
    return results;
}

@end
