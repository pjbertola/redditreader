//
//  ConnectionManager.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/15/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "ConnectionManager.h"
#import "RequestBuilder.h"
#import <AFNetworking/AFNetworking.h>

@interface ConnectionManager()
{
    AFURLSessionManager * _Nullable manager;
}

@end

@implementation ConnectionManager

+ (instancetype)sharedManager {
    static ConnectionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ConnectionManager alloc] init];
    });
    
    return sharedInstance;
}

-(instancetype)init {
    self = [super init];
    if (self)
    {
        manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

/**
 This method prforms a get to reddit's posts.

 @param after Is the last post id. Its use to get the next page.
 @param finishFunction callback with the posts, after and an error (if there is one).
 */
-(void)getPostsAfter:(NSString *_Nullable)after WithFinishBlock:(nullable void (^)(id  _Nullable responseObject, NSString *_Nullable after, NSError * _Nullable error))finishFunction{
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:[RequestBuilder createGetPostsAfter:after] completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        NSString *after = nil;
        if (!error){
            NSDictionary *dic = responseObject;
            after = [[dic objectForKey:@"data"] objectForKey:@"after"];
        }
        
        finishFunction(responseObject, after, error);
    }];
    
    [dataTask resume];
}

@end
