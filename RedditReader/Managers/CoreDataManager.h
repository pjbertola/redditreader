//
//  CoreDataManager.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CoreDataManager : NSObject

+ (instancetype)sharedManager;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

-(void)saveContext;
-(void)deleteAllPost;
-(void)deleteAllRecordsForEntity:(NSString*)entityName;
-(void)deleteObject:(NSManagedObject*)object;
- (NSArray *)getAllRecordsForEntity:(NSString *)entityName;
- (NSArray *)getAllPosts;
@end
