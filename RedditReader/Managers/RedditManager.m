//
//  RedditManager.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "RedditManager.h"
#import "ConnectionManager.h"
#import "PostModelView.h"

@implementation RedditManager

+ (instancetype)sharedManager {
    static RedditManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[RedditManager alloc] init];
        [self initialize];
        
    });
    
    return sharedInstance;
}

/**
 This method performs the post, and transforms the data recived to PostModelView objects.

 @param after Is the last post id. Its use to get the next page.
 @param finishFunction retuns the list of PostModelView objects, the after id an error (if there is one).
 */
-(void)getPostAfter:(NSString *)after withBlock:(nullable void (^)(NSMutableArray*  _Nullable list, NSString* _Nullable after, NSError* _Nullable error))finishFunction{
    [[ConnectionManager sharedManager]getPostsAfter:after WithFinishBlock:^(id  _Nullable responseObject, NSString * _Nullable after, NSError * _Nullable error) {
        
        if (!error) {
            NSString *_after;
            NSMutableArray *listResult = [NSMutableArray new];
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = [responseObject objectForKey:@"data"];
                _after = [dataDic objectForKey:@"after"];
                NSArray *children = [dataDic objectForKey:@"children"];

                for (NSDictionary *post in children) {
                    PostModelView *postMV = [PostModelView new];
                    postMV = [postMV initWithDictionary:[post objectForKey:@"data"]];
                    [listResult addObject:postMV];
                }
            }
            finishFunction(listResult,after,nil);
        }else {
            finishFunction(nil,nil,error);
        }

    }];
}

@end
