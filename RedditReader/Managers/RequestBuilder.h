//
//  RequestBuilder.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/15/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestBuilder : NSObject

+(NSMutableURLRequest*_Nullable) createGetPostsAfter:(NSString *_Nullable)after;
@end
