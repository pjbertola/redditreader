//
//  ConnectionManager.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/15/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject

+ (_Nonnull instancetype)sharedManager;

-(void)getPostsAfter:(NSString *_Nullable)after WithFinishBlock:(nullable void (^)(id  _Nullable responseObject, NSString *_Nullable after, NSError * _Nullable error))finishFunction;

@end
