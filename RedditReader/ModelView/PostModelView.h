//
//  PostModelView.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post+Model.h"

@interface PostModelView : NSObject

@property (nullable, nonatomic) NSString *author;
@property (nullable, nonatomic) NSDate *created_utc;
@property (nullable, nonatomic) NSString *id;
@property (nonatomic) int16_t num_comments;
@property (nullable, nonatomic) NSString *permalink;
@property (nullable, nonatomic) NSString *subreddit;
@property (nullable, nonatomic) NSString *thumbnail;
@property (nullable, nonatomic) NSString *title;
@property (nullable, nonatomic) Post *post;

-(instancetype _Nullable ) initWithDictionary:(NSDictionary *_Nonnull)values;
-(instancetype _Nullable )initWithModel:(Post *_Nullable)post;
@end
