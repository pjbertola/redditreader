//
//  PostModelView.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/16/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "PostModelView.h"
#import "Utils.h"
#import <UIKit/UIKit.h>

@implementation PostModelView

-(instancetype)initWithDictionary:(NSDictionary *)values
{
    if ([values objectForKey:@"id"]) self.id = [values objectForKey:@"id"];
    if ([values objectForKey:@"author"]) self.author = [values objectForKey:@"author"];
    if ([values objectForKey:@"num_comments"]) self.num_comments = [[values objectForKey:@"num_comments"] integerValue];
    if ([values objectForKey:@"permalink"]) self.permalink = [values objectForKey:@"permalink"];
    if ([values objectForKey:@"subreddit"]) self.subreddit = [values objectForKey:@"subreddit"];
    if ([values objectForKey:@"thumbnail"]) self.thumbnail = [values objectForKey:@"thumbnail"];
    if ([values objectForKey:@"title"]) self.title = [values objectForKey:@"title"];
    if ([values objectForKey:@"created_utc"]) self.created_utc = [NSDate dateWithTimeIntervalSince1970:[[values objectForKey:@"created_utc"] doubleValue]];

    //En otro contexto debería buscar si el modelo fue creado antes. Pero, a como está implementado, no es necesario.
    self.post = [Post factory];
    
    self = [self initModelWithDictionary:values];
    return self;
}

-(instancetype)initModelWithDictionary:(NSDictionary *)values
{
    if (self.post) {
        
        if ([values objectForKey:@"id"]) self.post.id = [values objectForKey:@"id"];
        if ([values objectForKey:@"author"]) self.post.author = [values objectForKey:@"author"];
        if ([values objectForKey:@"num_comments"]) self.post.num_comments = [[values objectForKey:@"num_comments"] integerValue];
        if ([values objectForKey:@"permalink"]) self.post.permalink = [values objectForKey:@"permalink"];
        if ([values objectForKey:@"subreddit"]) self.post.subreddit = [values objectForKey:@"subreddit"];
        if ([values objectForKey:@"thumbnail"]){
            self.post.thumbnail = [values objectForKey:@"thumbnail"];
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.post.thumbnail]];
            if (data) {
                [Utils writeToFileSystem:data withName:self.post.thumbnail];
            }
        }
        if ([values objectForKey:@"title"]) self.post.title = [values objectForKey:@"title"];
        if ([values objectForKey:@"created_utc"]) self.post.created_utc = [NSDate dateWithTimeIntervalSince1970:[[values objectForKey:@"created_utc"] doubleValue]];
    }
    
    return self;
}

-(instancetype)initWithModel:(Post *)post {
    self.post = post;
    if (post.id) self.id = post.id;
    if (post.author) self.author = post.author;
    if (post.num_comments) self.num_comments = post.num_comments;
    if (post.permalink) self.permalink = post.permalink;
    if (post.subreddit) self.subreddit = post.subreddit;
    if (post.thumbnail) self.thumbnail = post.thumbnail;
    if (post.title) self.title = post.title;
    if (post.created_utc) self.created_utc = post.created_utc;
    
    return self;
}

@end
