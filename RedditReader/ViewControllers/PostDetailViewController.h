//
//  PostDetailViewController.h
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/18/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "ViewController.h"
@class PostModelView;

@interface PostDetailViewController : ViewController
@property (nonatomic, strong) PostModelView *model;

@end
