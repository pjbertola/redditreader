//
//  PostDetailViewController.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/18/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "PostDetailViewController.h"
#import "PostModelView.h"
#import "Defines.h"

@interface PostDetailViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation PostDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:self.model.permalink]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
