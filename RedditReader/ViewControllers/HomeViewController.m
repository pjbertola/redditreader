//
//  HomeViewController.m
//  RedditReader
//
//  Created by Pablo Javier Bertola on 4/15/17.
//  Copyright © 2017 Pablo Javier Bertola. All rights reserved.
//

#import "HomeViewController.h"
#import "PostTableViewCell.h"
#import "CoreDataManager.h"
#import "RedditManager.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "Utils.h"
#import "PostModelView.h"
#import "PostDetailViewController.h"

@interface HomeViewController ()<PostTableViewCellDelegate>
{
    NSString *_after;
    __weak IBOutlet UITableView *table;
    NSMutableArray *_list;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    table.rowHeight = UITableViewAutomaticDimension;
    table.estimatedRowHeight = 90;
    [table addPullToRefreshWithActionHandler:^{
        [self getDataWithReload:YES];
    }];
    
    [table addInfiniteScrollingWithActionHandler:^{
        [self getDataWithReload:NO];
    }];
    
    [self getDataWithReload:YES];
}
-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    //This fix the problem that core data loads before the view.
    if (![Utils hasConnection]) {
        [table.pullToRefreshView stopAnimating];
        [table.infiniteScrollingView stopAnimating];
    }
    //this force the redraw in the rotation.
    [table reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_list count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PostTableViewCell"];
    PostModelView *postMV = [_list objectAtIndex:indexPath.row];
    [cell setModel:postMV];
    cell.delegate = self;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PostModelView *postMV = [_list objectAtIndex:indexPath.row];
    PostDetailViewController *viewController = [[PostDetailViewController alloc]init];
    
    viewController.model = postMV;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Manager Methods

/**
 retrive the objects to show.

 @param reload A bool that represent if it has to start from the begining or not.
 */
-(void)getDataWithReload:(BOOL)reload {
    if ([Utils hasConnection]) {
        //On-Line Mode
        if (reload) {
            _after = nil;
            _list = [NSMutableArray new];
            [[CoreDataManager sharedManager]deleteAllPost];
            
        }
        
        [[RedditManager sharedManager]getPostAfter:_after withBlock:^(NSMutableArray * _Nullable list, NSString * _Nullable after, NSError * _Nullable error) {
            
            if (!error) {
                _after = after;
                [_list addObjectsFromArray:list];
                
                [table reloadData];
            }else{
                //todo add alert
            }
            [table.pullToRefreshView stopAnimating];
            [table.infiniteScrollingView stopAnimating];
            [[CoreDataManager sharedManager]saveContext];
        }];
    }else{
        //Off-Line Mode
        [table.pullToRefreshView stopAnimating];
        [table.infiniteScrollingView stopAnimating];
        if ([_list count] == 0) {
            _list = [NSMutableArray new];
            NSArray* postObjList = [[CoreDataManager sharedManager]getAllPosts];
            for (NSManagedObject *post in postObjList) {
                PostModelView *postMV = [[PostModelView alloc]initWithModel:(Post*)post];
                [_list addObject:postMV];
            }
            
        }
        [table reloadData];
        
    }
    
}

#pragma mark - PostTableViewCellDelegate
-(void)tapButton:(PostModelView *)postMV{
    NSLog(@"%@", postMV.title);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
